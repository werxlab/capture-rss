=== Plugin Name ===
Contributors: hyperclock
Donate link: https://werxlab.com
Tags: rss, feed, news feed
Requires at least: 3.0.1
Tested up to: 3.4
Stable tag: 4.3
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Capture RSS is a plugin that adds the ability to import content from an external RSS feed.

== Description ==
Capture RSS is a plugin that adds the ability to import content from an external RSS feed.

=== Features ===
* Automatically creates a post for each feed captured.
* Displays an image.
* Creating posts on a specified date.
* Clicking on the posted list, allows you to jump to the original feed.
* Get multiple categories.
* Add excerpt of text to article.

== Installation ==
1. Upload `capture-rss.zip` to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress/ClassicPress
3. Appear under Settings menu.
4. Please enter the feed url you want to visit.
5. Click save.

== Frequently Asked Questions ==


== Screenshots ==


== Changelog ==

= 1.0.0 =
* Initial release.

== Upgrade Notice ==

== Arbitrary section ==
