<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://werxlab.com
 * @since      1.0.0
 *
 * @package    Capture_Rss
 * @subpackage Capture_Rss/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Capture_Rss
 * @subpackage Capture_Rss/includes
 * @author     JMColeman <hyperclock@werxlab.com>
 */
class Capture_Rss_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
