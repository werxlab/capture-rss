<?php

/**
 * Fired during plugin activation
 *
 * @link       https://werxlab.com
 * @since      1.0.0
 *
 * @package    Capture_Rss
 * @subpackage Capture_Rss/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Capture_Rss
 * @subpackage Capture_Rss/includes
 * @author     JMColeman <hyperclock@werxlab.com>
 */
class Capture_Rss_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
